import pygame
import random
import math
from configparser import ConfigParser

pygame.mixer.pre_init(44100, 16, 2, 4096)
pygame.init()
clock = pygame.time.Clock()

parser = ConfigParser()
parser.read('config.ini')
pX = int(parser.get('settings', 'pX'))
width = int(parser.get('settings', 'width'))
height = int(parser.get('settings', 'height'))
T = int(parser.get('settings', 'timer'))
vol = float(parser.get('settings', 'volume'))
nexlev = str(parser.get('message', 'levelinc'))
chngplr = str(parser.get('message', 'dead'))
numene = int(parser.get('settings', 'mons'))

pygame.mixer.music.load("bgmusic.mp3")
pygame.mixer.music.play(-1)
goose_sound = pygame.mixer.Sound("goose_sound.wav")
winlev = pygame.mixer.Sound("winlev.wav")
pygame.mixer.music.set_volume(vol)


def colortup(c):
    red, green, blue = c.split(',')
    red = int(red)
    green = int(green)
    blue = int(blue)
    return red, green, blue


l1 = colortup(parser.get('colors', 'lgrey'))
l2 = colortup(parser.get('colors', 'lyellow'))
l3 = colortup(parser.get('colors', 'lpink'))
l4 = colortup(parser.get('colors', 'lblue'))
l5 = colortup(parser.get('colors', 'lgreen'))
good = colortup(parser.get('colors', 'green'))
bad = colortup(parser.get('colors', 'red'))
lake = colortup(parser.get('colors', 'bg'))
land = colortup(parser.get('colors', 'lawn'))
gold = colortup(parser.get('colors', 'gold'))
black = colortup(parser.get('colors', 'black'))

screen = pygame.display.set_mode((width, height), pygame.FULLSCREEN)
playerX = pX
playerY = height - 36
speed = 3
rep = (height - 50) // 60
flesh = pygame.image.load('meat.png')
playerImg1 = pygame.image.load('hen.png')
playerImg2 = pygame.image.load('goose2.png')
playerImg = playerImg1
pygame.display.set_caption("Cross the Road?")
enemyImg = pygame.image.load('spooky.png')
enemy2Img = pygame.image.load('dragon.png')
playerX_change = 0
playerY_change = 0

score_val1 = 0
perma1 = 0
score_val2 = 0
perma2 = 0
ctrl = 1
cc = 0
levhi = 1
timer = T
font = pygame.font.Font('freesansbold.ttf', 24)
fontbig = pygame.font.SysFont('comicsansms', 72)

colorlist = [l1, l2, l3, l4, l5]


def player(x, y):
    screen.blit(playerImg, (x, y))


def score_p1():
    score = font.render("Score_P1: " + str(score_val1 + perma1), True,
                        (0, 0, 0))
    screen.blit(score, (10, height - 30))


def score_p2():
    score = font.render("Score_P2: " + str(score_val2 + perma2), True,
                        (0, 0, 0))
    screen.blit(score, (width - 180, 10))


def note(m, color, bfil=colorlist[cc]):
    global cc
    screen.fill(bfil)
    cc = (cc + 1) % 5
    msg = fontbig.render(m, True, color)
    wth = msg.get_width()
    hgt = msg.get_height()
    screen.blit(msg, (width / 2 - wth // 2, height / 2 - hgt // 2))
    pygame.display.update()


def result():
    if perma1 > perma2:
        note("Player 1 Wins!", gold, black)
    if perma2 > perma1:
        note("Player 2 wins!", gold, black)
    if perma1 == perma2:
        note("...Tie...", gold, black)
    pygame.time.delay(1500)


class EnemyDrg:
    def __init__(self, x, y, direc, vel):
        self.x = x
        self.y = y
        self.direc = direc
        self.vel = vel

    def display(self):
        screen.blit(enemy2Img, (self.x, self.y))

    def collision(self):
        distance = math.sqrt(
            math.pow(self.x - playerX, 2) + math.pow(self.y - playerY, 2))
        if distance < 25:
            return True
        else:
            return False


class Enemy:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def display(self):
        screen.blit(enemyImg, (self.x, self.y))

    def collision(self):
        distance = math.sqrt(
            math.pow(self.x - playerX, 2) + math.pow(self.y - playerY, 2))
        if distance < 25:
            return True
        else:
            return False


enelist = []
ene2list = []
randompos = []
randomspeed = []


def change():
    global randomspeed, randompos, i
    randompos = [random.randrange(5, width - 37, 32) for i in range(20)]
    randomspeed = [random.uniform(1, 4) for i in range(10)]


change()


def initial():
    global playerX, speed, enelist, ene2list, levhi, i
    playerX = pX
    speed = 3
    levhi = 1
    ene2list = []
    j = 70
    for i in range(1, rep):
        p2 = EnemyDrg(randompos[i], j, 1, randomspeed[i])
        j += 60
        ene2list.append(p2)
    enelist = []
    j = 0
    for i in range(40, 40 + 60 * (rep - 1) + 1, 60):
        for j1 in range(1, numene):
            p1 = Enemy(randompos[j], i)
            enelist.append(p1)
            j = (j + 1) % 20


initial()


def levelup():
    pygame.mixer.music.pause()
    pygame.mixer.Sound.play(winlev)
    global ene2list, timer, ctrl, playerY, perma2, perma1
    global score_val1, score_val2, speed, levhi, timer, T, good
    perma1 += score_val1
    score_val1 = 0
    perma2 += score_val2
    score_val2 = 0
    speed -= 0.5
    levhi += 1
    note(nexlev, good)
    pygame.time.delay(600)
    note("Level: " + str(levhi), good)
    pygame.time.delay(600)

    if speed == 1.5:
        speed = 5  # after 3 levels speed will suddenly go up
    if perma1 > 700 and T >= 60 * 8:
        T -= 60 * 3  # reduce 3 seconds for reaching other side
    for enemyobj in ene2list:
        enemyobj.vel += 1  # increase in difficultly
    if ctrl == 1:
        playerY = height - 36
        perma1 += 20 + (timer // 10)  # bonus for clearing level + time bonus
    else:
        playerY = 0
        perma2 += 20 + (timer // 10)  # bonus for clearing level + time bonus
    timer = T
    pygame.mixer.music.unpause()


def kill():
    pygame.mixer.music.pause()
    pygame.mixer.Sound.play(goose_sound)
    global playerImg, flesh, playerY, ctrl, timer
    pygame.time.delay(800)
    playerImg = flesh
    player(playerX, playerY)
    pygame.display.update()
    pygame.time.delay(800)
    global perma1, perma2, score_val1, score_val2, playerImg2, playerImg1
    timer = T
    note(chngplr, bad)
    pygame.time.delay(700)
    initial()
    change()
    if ctrl == 1:
        perma1 = perma1 + score_val1
        score_val1 = 0
        ctrl = 2
        playerY = 0
        playerImg = playerImg2
    else:
        perma2 = perma2 + score_val2
        score_val2 = 0
        ctrl = 1
        playerY = height - 36
        playerImg = playerImg1
    pygame.mixer.music.unpause()


# game loop
# general note: a player continues to play as long as s/he does not collide,
# after which the other player will play
# and each time the level for that that player will be reset to 1
running = True
pres = 0
while running:
    screen.fill(lake)
    timer -= 1  # after 10 seconds time penalty applies else bonus

    for i in range(40, 41 + (rep - 1) * 60, 60):
        rectY = pygame.Rect(0, i - 5, width,
                            30 + 10)  # for broader resting region
        pygame.draw.rect(screen, land, rectY)

    k = 0
    for enemyobj in enelist:
        enemyobj.display()
        if levhi > 5:
            k1 = (2 * (k % 2)) - 1
            enemyobj.y = 40 + ((enemyobj.y - 40 + k1) % (60 * (rep - 1)))
        if enemyobj.collision():
            print("Player" + str(ctrl) + " collided with static")
        k += 1

    for enemyobj in ene2list:
        if enemyobj.x > (width - 50):
            enemyobj.direc = -1
        if enemyobj.x < 30:
            enemyobj.direc = 1

        enemyobj.x += enemyobj.direc * enemyobj.vel
        enemyobj.display()
        if enemyobj.collision():
            print("Player" + str(ctrl) + " collided with dynamic")

    for enemyobj in ene2list + enelist:
        if enemyobj.collision():
            kill()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            result()
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = -speed
            if event.key == pygame.K_RIGHT:
                playerX_change = speed
            if event.key == pygame.K_UP:
                playerY_change = -speed
            if event.key == pygame.K_DOWN:
                playerY_change = speed
            # to exit game from full screen
            if event.key == pygame.K_ESCAPE:
                result()
                running = False
            # cheat code for jump
            if event.key == pygame.K_SPACE and pres == 0:
                pres = 1
                if ctrl == 1:
                    playerY_change = -60
                else:
                    playerY_change = 60
            # cheat code to temporarily freeze enemies till key is held
            if event.key == pygame.K_BACKSPACE:
                for enemyobj in ene2list:
                    enemyobj.direc = 0
            # cheat code for instant death
            if event.key == pygame.K_DELETE:
                kill()

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = 0
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                playerY_change = 0
            if event.key == pygame.K_SPACE:
                pres = 0
            if event.key == pygame.K_BACKSPACE:  # release enemies
                k = 0
                for enemyobj in ene2list:
                    k += 1
                    enemyobj.direc = 2 * (k % 2) - 1

    if running is False:
        break

    playerX += playerX_change
    playerY += playerY_change

    if pres == 1:
        pres = 2
        playerY_change = 0

    playerX = max(0, playerX)
    playerX = min(width - 32, playerX)
    playerY = max(0, playerY)
    playerY = min(height - 36, playerY)

    if ctrl == 1:
        if playerY < 20:
            levelup()
            continue
    else:
        if playerY > 600:
            levelup()
            continue

    player(playerX, playerY)
    # print(playerY)

    if ctrl == 1:
        sc = ((rep * 60 - 20 - playerY) // 60) * 15 + (
                ((rep * 60 - 20 - playerY) % 60) // 35) * 5
        score_val1 = max(score_val1, sc)
        score_p1()
    else:
        sc = ((playerY - 40) // 60) * 15 + (((playerY - 40) % 60) // 35) * 5
        score_val2 = max(score_val2, sc)
        score_p2()

    pygame.display.update()
    clock.tick(60)
